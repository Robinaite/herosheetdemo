using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiScript : MonoBehaviour
{
    [SerializeField]
    public GameLoop gameLoopRef;

    [SerializeField]
    public TextMeshProUGUI notesAmount;
    [SerializeField]
    public TextMeshProUGUI time;

    [SerializeField]
    public GameObject HelpWindow;

    private void Update()
    {
        notesAmount.text = "Amount TO attack:" + gameLoopRef.sumTotalNoteLengthAmount + "/" + gameLoopRef.measureLength;

        if(gameLoopRef.sumTotalNoteLengthAmount == gameLoopRef.measureLength)
        {
            notesAmount.text += " - Attack by clicking Spacebar!";
        }
        else
        {
            notesAmount.text += " - Adjust notes sizes.";
        }

        if(gameLoopRef.wonLost == 1)
        {
            notesAmount.text = "You Won! Click R to restart.";
        }
        else if(gameLoopRef.wonLost == -1)
        {
            notesAmount.text = "You Lost! Click R to restart.";
        }

        if (gameLoopRef.gameStart)
        {
            time.text = Mathf.Clamp(Mathf.CeilToInt(gameLoopRef.time),1,4) + "/" + gameLoopRef.measureTimeInSeconds;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            HelpWindow.SetActive(!HelpWindow.activeInHierarchy);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
