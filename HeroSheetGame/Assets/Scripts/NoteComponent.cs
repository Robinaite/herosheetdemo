﻿using System.Collections;
using TMPro;
using UnityEngine;

public class NoteComponent : MonoBehaviour, System.IComparable<NoteComponent>
{
    public int position;
    public int line = 0;
    public NoteScriptableObject noteData;

    public GameObject AttackMode;
    public GameObject buffMode;

    public GameObject attack;
    public TextMeshPro textAMount;

    public TextMeshPro bonusAmount;

    public SpriteRenderer SpriteAttack;
    public SpriteRenderer SpriteBuff;

    public int CompareTo(NoteComponent other)
    {
        if (position == other.position) return 0;
        if (position > other.position) return 1;
        return -1;
    }

    private void Start()
    {
        textAMount.text = noteData.noteSize.ToString();
        bonusAmount.text = noteData.GetMultiplier().ToString();
    }

    public void SetLayerOrder(int order)
    {
        SpriteAttack.sortingOrder = order;
        SpriteBuff.sortingOrder = order;
    }

    public bool IsAttackMode()
    {
        return AttackMode.activeInHierarchy;
    }

    public void ToggleAttackMode()
    {
        buffMode.SetActive(AttackMode.activeInHierarchy);
        AttackMode.SetActive(!AttackMode.activeInHierarchy);
    }

    public void ShowNoteDamage(int damage)
    {
        textAMount.text = damage.ToString();
        bonusAmount.text = "";
    }

    public void ShowAttack()
    {
        StartCoroutine(ShowAttackTime());
    }

    public IEnumerator ShowAttackTime()
    {
        attack.SetActive(true);
        yield return new WaitForSeconds(10);
        attack.SetActive(false);
    }
}