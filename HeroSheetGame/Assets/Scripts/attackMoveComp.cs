using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attackMoveComp : MonoBehaviour
{

    bool startMoving = false;
    [SerializeField]
    float speed = 1;
    // Start is called before the first frame update
    private void OnEnable()
    {
        startMoving = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (startMoving)
        {
            transform.localPosition += new Vector3(Time.deltaTime*speed, 0, 0);
        }
    }
}
