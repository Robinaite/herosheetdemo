using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemiesComponent : MonoBehaviour
{
    public int health = 250;

    private int runtimeHealth = 1;

    private GameLoop gameLoopRef;
    public TextMeshPro healthText;

    private void Start()
    {
        runtimeHealth = health;
        healthText.text = health.ToString();
        gameLoopRef = FindObjectOfType<GameLoop>();
    }

    public void TakeDamage(int amount)
    {
        health -= amount;
        healthText.text = health.ToString();
        if (health <= 0)
        {
            gameLoopRef.DeleteEnemy(this);
        }
    }

}
