using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoop : SerializedMonoBehaviour
{
    [SerializeField]
    public int beatsTop = 3;

    [SerializeField]
    public int noteTypeBottom = 4;

    [SerializeField]
    public int bpm = 60;

    [ShowInInspector]
    public int measureLength = 0;

    [ShowInInspector]
    public float measureTimeInSeconds;

    public float measureTimeLeft;

    [SerializeField]
    public List<NoteScriptableObject> notesToBeUsed;
    [ShowInInspector,SerializeField]
    public List<List<EnemiesComponent>> enemiesToSpawn = new List<List<EnemiesComponent>>();

    [SerializeField]
    public Transform notesParent;
    [SerializeField]
    public Transform enemiesParent;
    [SerializeField]
    public Transform selector;

    //Position on measure,Note type(gameObject)>
    //Line 1
    public List<NoteComponent> notesOnSingleLine = new List<NoteComponent>();
    public List<List<EnemiesComponent>> enemiesOnLines = new List<List<EnemiesComponent>>();

    [SerializeField]
    public bool gameStart = false;
    public int sumTotalNoteLengthAmount = 0;

    public int prevUpdatePos = -1;
    [ShowInInspector,ReadOnly]
    public float time = 0;

    [ShowInInspector]
    public int noteToUpdate = 0;

    public int currentLine = 0;

    public int wonLost = 0;

    private int boostedDamageMultiplier = 0;

    private void Start()
    {
        NoteScriptableObject noteSO = notesToBeUsed.Find(elm => elm.noteLength == noteTypeBottom);
        measureLength = (NoteScriptableObject.wholeNoteLength / noteSO.noteLength)*beatsTop;

        int totalNotePerMinute = noteSO.noteSize * bpm;
        float amountInMinutePerMeasure = measureLength / (float)totalNotePerMinute;

        measureTimeInSeconds = measureTimeInSeconds = amountInMinutePerMeasure * 60;

        measureTimeLeft = measureTimeInSeconds;
        gameStart = false;

        AddNote(noteSO);
        for (int i = 0; i < enemiesToSpawn.Count; i++)
        {
            List<EnemiesComponent> newList = new List<EnemiesComponent>();
            enemiesOnLines.Add(newList);
            List<EnemiesComponent> line = enemiesToSpawn[i];
            foreach (EnemiesComponent enemyComp in line)
            {
                AddEnemy(i,enemyComp.gameObject);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(sumTotalNoteLengthAmount != measureLength)
            {
                Debug.Log("Total is too much or too low, wont start game! Sum of notes = " + sumTotalNoteLengthAmount + " Amount Necessary = " + measureLength);
            }
            else
            {
                gameStart = true;
            }
        }

        if (gameStart)
        {
            int newPos = Mathf.FloorToInt(Mathf.Lerp(0, measureLength, time/measureTimeInSeconds));
            //Debug.Log("Time: " + time + " percentage = "+ time / measureTimeInSeconds + " newPos = " + newPos + " prevUpdatePos =" + prevUpdatePos);

            for (int i = 0; i < notesOnSingleLine.Count; i++)
            {
                NoteComponent noteComp = notesOnSingleLine[i];
                if (noteComp.position <= newPos && (noteComp.position > prevUpdatePos))
                {
                    Debug.Log("Executing Note: " + noteComp.ToString() + " On Position: " + newPos);

                    if (noteComp.IsAttackMode())
                    {
                        noteComp.ShowAttack();
                        int damageAmount = 0;
                        if(boostedDamageMultiplier > 0)
                        {
                            damageAmount = noteComp.noteData.noteSize * boostedDamageMultiplier;
                        }
                        else
                        {
                            damageAmount = noteComp.noteData.noteSize;
                        }
                        noteComp.ShowNoteDamage(damageAmount);
                        if(enemiesOnLines[noteComp.line].Count > 0)
                        {
                            enemiesOnLines[noteComp.line][0].TakeDamage(damageAmount);
                        }
                        boostedDamageMultiplier = 0;
                    }
                    else
                    {
                        if(i == 0)
                        {
                            boostedDamageMultiplier = noteComp.noteData.GetMultiplier();
                        }
                        else
                        {
                            if(!notesOnSingleLine[i - 1].IsAttackMode()) //Previous note is buff note.
                            {
                                if (notesOnSingleLine[i - 1].line == noteComp.line) // Im on the same line as the previous buff note.
                                {
                                    boostedDamageMultiplier += noteComp.noteData.GetMultiplier();
                                }
                                else
                                {
                                    //Im on a different line than previous buff note.
                                    boostedDamageMultiplier *= noteComp.noteData.GetMultiplier();
                                }
                            }
                            else
                            {
                                //else just set multiplier to current note value;
                                boostedDamageMultiplier = noteComp.noteData.GetMultiplier();
                            }

                           
                        }

                        Debug.Log("Boosted Damage Multipler: " + boostedDamageMultiplier);
                    }
                }
            }

            if(newPos >= measureLength)
            {
                bool lost = false;
                foreach (List<EnemiesComponent> enemyCompList in enemiesOnLines)
                {
       
                    lost |= enemyCompList.Count != 0;
                }

                if (lost)
                {
                    Debug.Log("Boo, you loose.");
                    wonLost = -1;
                }
                else
                {
                    Debug.Log("Congrats you won!");
                    wonLost = 1;
                }
            }

            time += Time.deltaTime;
            prevUpdatePos = newPos;

            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        else
        {
            sumTotalNoteLengthAmount = 0;
            foreach (NoteComponent noteComp in notesOnSingleLine)
            {
                sumTotalNoteLengthAmount += noteComp.noteData.noteSize;
            }

            if (notesOnSingleLine.Count > 0)
            {
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    NoteComponent noteComp = notesOnSingleLine[noteToUpdate];
                    ChangeNote(noteComp, notesToBeUsed[0]);
                }
                else if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    NoteComponent noteComp = notesOnSingleLine[noteToUpdate];
                    ChangeNote(noteComp, notesToBeUsed[1]);
                }
                else if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    NoteComponent noteComp = notesOnSingleLine[noteToUpdate];
                    ChangeNote(noteComp, notesToBeUsed[2]);
                }
                else if (Input.GetKeyDown(KeyCode.Alpha4))
                {
                    NoteComponent noteComp = notesOnSingleLine[noteToUpdate];
                    ChangeNote(noteComp, notesToBeUsed[3]);
                } 
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    if (noteToUpdate + 1 < notesOnSingleLine.Count)
                    {
                        noteToUpdate++;
                        Debug.Log("Note TO update: " + noteToUpdate);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    if (noteToUpdate - 1 >= 0)
                    {
                        noteToUpdate--;
                        Debug.Log("Note TO update: " + noteToUpdate);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (currentLine - 1 >= 0)
                    {
                        currentLine--;
                        Debug.Log("Current Line: " + currentLine);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (currentLine + 1 < 5)
                    {
                        currentLine++;
                        Debug.Log("Current Line: " + currentLine);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.C))
                {
                    NoteComponent noteComp = notesOnSingleLine[noteToUpdate];
                    noteComp.ToggleAttackMode();
                }
                else if (Input.GetKeyDown(KeyCode.D))
                {
                    NoteComponent noteComp = notesOnSingleLine[noteToUpdate];
                    if(noteComp.line == currentLine)
                    {
                        RemoveNote(noteComp);
                        if (notesOnSingleLine.Count == 0)
                        {
                            noteToUpdate = 0;
                        }
                        else
                        {
                            noteToUpdate = notesOnSingleLine.Count - 1;
                        }
                        Debug.Log("Note TO update: " + noteToUpdate);
                    }
                }
            } 
            if (Input.GetKeyDown(KeyCode.A))
            {
                AddNote(notesToBeUsed[0]);
            }
            UpdateSelectorPosition();
        }
    }


    public void UpdateSelectorPosition()
    {
        selector.localPosition = new Vector3(notesOnSingleLine[noteToUpdate].position / (float)250, currentLine, 0);
    }

    public void AddNote(NoteScriptableObject noteData)
    {
        GameObject newNote = Instantiate(noteData.notePrefab, notesParent);
        NoteComponent noteComp = newNote.GetComponent<NoteComponent>();
        noteComp.line = currentLine;
        if(notesOnSingleLine.Count > 0)
        {
            noteComp.position = notesOnSingleLine[noteToUpdate].position + notesOnSingleLine[noteToUpdate].noteData.noteSize -1;
        }
        else
        {
            noteComp.position = 0;
        }
        noteComp.SetLayerOrder(notesOnSingleLine.Count);
        newNote.transform.localPosition = new Vector3(noteComp.position/(float)250, noteComp.line, 0);
        notesOnSingleLine.Add(noteComp);
        notesOnSingleLine.Sort();
        UpdatePositionsAfterChange();
    }

    public void ChangeNote(NoteComponent oldNote, NoteScriptableObject newNote)
    {
        int position = oldNote.position;

        if(oldNote.line == currentLine)
        {
            notesOnSingleLine.Remove(oldNote);
            Destroy(oldNote.gameObject);

            GameObject newNoteGO = Instantiate(newNote.notePrefab, notesParent);
            NoteComponent noteComp = newNoteGO.GetComponent<NoteComponent>();
            noteComp.position = position;
            noteComp.line = currentLine;
            if (!oldNote.IsAttackMode())
            {
                noteComp.ToggleAttackMode();
            }

            notesOnSingleLine.Add(noteComp);
            notesOnSingleLine.Sort();
        }
        else
        {
            oldNote.line = currentLine;
        }

        UpdatePositionsAfterChange();
    }

    public void RemoveNote(NoteComponent oldNote)
    {
        if(notesOnSingleLine.Count > 1)
        {
            notesOnSingleLine.Remove(oldNote);
            Destroy(oldNote.gameObject);

            UpdatePositionsAfterChange();
        }
    }

    private void UpdatePositionsAfterChange()
    {
        //Update positions after changing a note.
        notesOnSingleLine[0].position = 0;
        notesOnSingleLine[0].transform.localPosition = new Vector3(notesOnSingleLine[0].position / (float)250, notesOnSingleLine[0].line, 0);
        notesOnSingleLine[0].SetLayerOrder(0);

        for (int i = 1; i < notesOnSingleLine.Count; i++)
        {
            notesOnSingleLine[i].position = notesOnSingleLine[i - 1].position + notesOnSingleLine[i - 1].noteData.noteSize;  
            notesOnSingleLine[i].transform.localPosition = new Vector3(notesOnSingleLine[i].position / (float)250, notesOnSingleLine[i].line, 0);
            notesOnSingleLine[i].SetLayerOrder(i);
        }
    }

    private void AddEnemy(int line,GameObject enemiesPrefab)
    {
        GameObject newEnemy = Instantiate(enemiesPrefab, enemiesParent);
        EnemiesComponent enemyComp = newEnemy.GetComponent<EnemiesComponent>();
        enemyComp.transform.localPosition = new Vector3(enemiesOnLines[line].Count * 1.1f, line, 0);
        enemiesOnLines[line].Add(enemyComp);
    }

    public void DeleteEnemy(EnemiesComponent enemiesComponent)
    {
        for (int i = 0; i < enemiesOnLines.Count; i++)
        {
            List<EnemiesComponent> line = enemiesOnLines[i];
            foreach (EnemiesComponent enemyComp in line)
            {
                if(enemyComp == enemiesComponent)
                {
                    line.Remove(enemiesComponent);
                    Destroy(enemiesComponent.gameObject);
                    return;
                }
            }
        }
    }
}
