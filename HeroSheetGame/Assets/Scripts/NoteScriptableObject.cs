using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Note", menuName = "Notes/CreateNote", order = 1)]
public class NoteScriptableObject : SerializedScriptableObject
{
    [ShowInInspector]
    public static int wholeNoteLength = 1000;

    [SerializeField]
    public GameObject notePrefab;

    [ShowInInspector]
    public int NoteSizeInput
    {
        get { return wholeNoteLength/ noteSize; }
        set {
            noteSize = value == 0 ? wholeNoteLength / 1 : wholeNoteLength / value;
            noteLength = value;
        }
    }
    [SerializeField,ReadOnly]
    public int noteSize = wholeNoteLength;

    [SerializeField, ReadOnly]
    public int noteLength = 1;

    [SerializeField]
    public int multiplierValue;

    public int GetMultiplier()
    {
        return multiplierValue;
    }

}
